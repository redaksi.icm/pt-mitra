-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 30 Sep 2020 pada 18.58
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_stok_barang`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `story_barang_masuk_s`
--

CREATE TABLE `story_barang_masuk_s` (
  `id_story_barang_masuk` int(10) NOT NULL,
  `kd_BK` int(10) DEFAULT NULL,
  `jumlah_masuk_s` double DEFAULT NULL,
  `netto_masuk_s` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `story_barang_produksi_s`
--

CREATE TABLE `story_barang_produksi_s` (
  `id_story_produksi_masuk` int(10) NOT NULL,
  `kd_bm_produksi` int(10) DEFAULT NULL,
  `jumlah_masuk_p` double DEFAULT NULL,
  `netto_masuk_p` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_barang_keluar`
--

CREATE TABLE `tbl_barang_keluar` (
  `kd_bm` int(10) NOT NULL,
  `kd_bk` int(10) DEFAULT NULL,
  `tanggal_bk` date DEFAULT NULL,
  `jumlah_bk` double DEFAULT NULL,
  `tujuan_keluar` text DEFAULT NULL,
  `netto_keluar` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_barang_keluar_produksi`
--

CREATE TABLE `tbl_barang_keluar_produksi` (
  `kd_keluar_produksi` int(10) NOT NULL,
  `kd_bm_produksi` int(10) DEFAULT NULL,
  `jumlah_keluar_produksi` double DEFAULT NULL,
  `netto_keluar_produksi` double DEFAULT NULL,
  `tgl_keluar_produksi` date DEFAULT NULL,
  `tujuan_keluar_produksi` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_barang_masuk`
--

CREATE TABLE `tbl_barang_masuk` (
  `kd_BK` int(10) NOT NULL,
  `kd_suplayer` int(10) DEFAULT NULL,
  `kode_barang` int(10) DEFAULT NULL,
  `tanggal_BM` date DEFAULT NULL,
  `jumlah_BM` double DEFAULT NULL,
  `netto` double DEFAULT NULL,
  `hasil_test` varchar(25) DEFAULT NULL,
  `harga` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_barang_masuk_produksi`
--

CREATE TABLE `tbl_barang_masuk_produksi` (
  `kd_bm_produksi` int(10) NOT NULL,
  `kd_produksi` int(10) DEFAULT NULL,
  `tgl_masuk_produksi` date DEFAULT NULL,
  `jumlah_masuk` double DEFAULT NULL,
  `netto_masuk` double DEFAULT NULL,
  `hasil_test` varchar(20) DEFAULT NULL,
  `ket_masuk` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_hasil_produksi`
--

CREATE TABLE `tbl_hasil_produksi` (
  `kd_produksi` int(10) NOT NULL,
  `nama_barang_produksi` varchar(20) DEFAULT NULL,
  `jumlah_barang_pro` double DEFAULT NULL,
  `netto` double DEFAULT NULL,
  `ket_p` text DEFAULT NULL,
  `satuan_produksi` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_hasil_produksi`
--

INSERT INTO `tbl_hasil_produksi` (`kd_produksi`, `nama_barang_produksi`, `jumlah_barang_pro`, `netto`, `ket_p`, `satuan_produksi`) VALUES
(12, 'Ndak Tau do', 0, 0, 'oke', 'Drum'),
(13, 'Mie Instal Sedap', 0, 0, 'oke', 'Drum');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_info`
--

CREATE TABLE `tbl_info` (
  `kd_info` int(20) NOT NULL,
  `judul` varchar(50) DEFAULT NULL,
  `gambar` text DEFAULT NULL,
  `tgl_info` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_info`
--

INSERT INTO `tbl_info` (`kd_info`, `judul`, `gambar`, `tgl_info`) VALUES
(5, 'sadsdsa', 'HST2V1W-laravel-logo.png', '2020-09-08'),
(7, 'Produksi Mulai Tanggal 20 Septembar', '5XYDA3G-I_have_an_dea_desktop_background_by_ZerCareer_2048x2048.webp', '2020-09-22'),
(8, 'Penambahan Produksi', '2D9ZATJ-67799570-programming-wallpapers.png', '2020-09-23');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_opname`
--

CREATE TABLE `tbl_opname` (
  `No_opname` int(10) NOT NULL,
  `tgl_opname` date DEFAULT NULL,
  `jumlah` double DEFAULT NULL,
  `netto` double DEFAULT NULL,
  `satuan` varchar(20) DEFAULT NULL,
  `kode_barang` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_stok_barang`
--

CREATE TABLE `tbl_stok_barang` (
  `kode_barang` int(10) NOT NULL,
  `nama_barang` varchar(100) DEFAULT NULL,
  `jumlah_barang_stok` double(20,0) DEFAULT NULL,
  `netto_stok` double DEFAULT NULL,
  `ket` text DEFAULT NULL,
  `satuan_stok` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_stok_barang`
--

INSERT INTO `tbl_stok_barang` (`kode_barang`, `nama_barang`, `jumlah_barang_stok`, `netto_stok`, `ket`, `satuan_stok`) VALUES
(7, 'Minyak Tanah', 0, 0, 'Ok', 'Coli'),
(9, 'Minyak Bensin', 0, 0, 'ok', 'Drum');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_suplayer`
--

CREATE TABLE `tbl_suplayer` (
  `kd_suplayer` int(10) NOT NULL,
  `nama_suplayer` varchar(200) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `jk` varchar(10) DEFAULT NULL,
  `no_hp` double DEFAULT NULL,
  `status` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_suplayer`
--

INSERT INTO `tbl_suplayer` (`kd_suplayer`, `nama_suplayer`, `alamat`, `jk`, `no_hp`, `status`) VALUES
(1, 'Hariadi Raharja', 'makasar', '1', 532489, '1'),
(2, 'Ramlan Toko', 'dwwqeqeqweqwew', '2', 942347384, '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id_user` int(10) NOT NULL,
  `user` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `no_hp` int(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `jabatan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_user`
--

INSERT INTO `tbl_user` (`id_user`, `user`, `password`, `nama`, `alamat`, `no_hp`, `email`, `jabatan`) VALUES
(2, 'karyawan', '12345', 'KaryawanB', 'padang', 90, 'ka@.com', 'Karyawan'),
(3, 'ayu', '12345', 'AyuA', 'Padang', 35354545, 'k@gmail.com', 'Marketing'),
(4, 'sabar', '12345', 'Sabar', 'Padang', 35354545, 'k@gmail.com', 'Pimpinan'),
(5, '2', '2', 'sadasds', 'sadasd', 21, 'k@gmail.com', 'Karyawan'),
(6, '1', '1', '1', '1', 16, 'k@gmail.com', 'Karyawan'),
(7, '23', '23', 'sdsadsad', 'asdasdas', 2147483647, 'k@gmail.com', 'Marketing');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `story_barang_masuk_s`
--
ALTER TABLE `story_barang_masuk_s`
  ADD PRIMARY KEY (`id_story_barang_masuk`);

--
-- Indeks untuk tabel `story_barang_produksi_s`
--
ALTER TABLE `story_barang_produksi_s`
  ADD PRIMARY KEY (`id_story_produksi_masuk`);

--
-- Indeks untuk tabel `tbl_barang_keluar`
--
ALTER TABLE `tbl_barang_keluar`
  ADD PRIMARY KEY (`kd_bm`) USING BTREE;

--
-- Indeks untuk tabel `tbl_barang_keluar_produksi`
--
ALTER TABLE `tbl_barang_keluar_produksi`
  ADD PRIMARY KEY (`kd_keluar_produksi`);

--
-- Indeks untuk tabel `tbl_barang_masuk`
--
ALTER TABLE `tbl_barang_masuk`
  ADD PRIMARY KEY (`kd_BK`) USING BTREE;

--
-- Indeks untuk tabel `tbl_barang_masuk_produksi`
--
ALTER TABLE `tbl_barang_masuk_produksi`
  ADD PRIMARY KEY (`kd_bm_produksi`);

--
-- Indeks untuk tabel `tbl_hasil_produksi`
--
ALTER TABLE `tbl_hasil_produksi`
  ADD PRIMARY KEY (`kd_produksi`);

--
-- Indeks untuk tabel `tbl_info`
--
ALTER TABLE `tbl_info`
  ADD PRIMARY KEY (`kd_info`);

--
-- Indeks untuk tabel `tbl_opname`
--
ALTER TABLE `tbl_opname`
  ADD PRIMARY KEY (`No_opname`);

--
-- Indeks untuk tabel `tbl_stok_barang`
--
ALTER TABLE `tbl_stok_barang`
  ADD PRIMARY KEY (`kode_barang`) USING BTREE;

--
-- Indeks untuk tabel `tbl_suplayer`
--
ALTER TABLE `tbl_suplayer`
  ADD PRIMARY KEY (`kd_suplayer`);

--
-- Indeks untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `story_barang_masuk_s`
--
ALTER TABLE `story_barang_masuk_s`
  MODIFY `id_story_barang_masuk` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `story_barang_produksi_s`
--
ALTER TABLE `story_barang_produksi_s`
  MODIFY `id_story_produksi_masuk` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tbl_barang_keluar`
--
ALTER TABLE `tbl_barang_keluar`
  MODIFY `kd_bm` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `tbl_barang_keluar_produksi`
--
ALTER TABLE `tbl_barang_keluar_produksi`
  MODIFY `kd_keluar_produksi` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `tbl_barang_masuk`
--
ALTER TABLE `tbl_barang_masuk`
  MODIFY `kd_BK` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT untuk tabel `tbl_barang_masuk_produksi`
--
ALTER TABLE `tbl_barang_masuk_produksi`
  MODIFY `kd_bm_produksi` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `tbl_hasil_produksi`
--
ALTER TABLE `tbl_hasil_produksi`
  MODIFY `kd_produksi` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `tbl_info`
--
ALTER TABLE `tbl_info`
  MODIFY `kd_info` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `tbl_opname`
--
ALTER TABLE `tbl_opname`
  MODIFY `No_opname` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tbl_stok_barang`
--
ALTER TABLE `tbl_stok_barang`
  MODIFY `kode_barang` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `tbl_suplayer`
--
ALTER TABLE `tbl_suplayer`
  MODIFY `kd_suplayer` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
