<?php
$no = 1;
$tampil = mysqli_query($connect, "SELECT * FROM tbl_opname inner join tbl_stok_barang on tbl_opname.kode_barang=tbl_stok_barang.kode_barang where No_opname='$_GET[id]'");
foreach ($tampil as $data) {
?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        EDIT OPNAME
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <form action="../action/edit_opname.php?id=<?php echo $data['No_opname']; ?>" method="post">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Nama Barang</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" readonly value="<?php echo $data['nama_barang']; ?>" name="barang" />
                                    </div>
                                </div>
                                <label>Tgl Opname</label>
                                <div class="form-group">
                                    <div class="input-group date" id="bs_datepicker_component_container">
                                        <div class="form-line">
                                            <input type="text" class="form-control" value="<?php echo date('m/d/Y', strtotime($data['tgl_opname'])); ?>" name="tgl_opname">
                                        </div>
                                        <span class="input-group-addon">
                                            <i class="material-icons">date_range</i>
                                        </span>
                                    </div>
                                </div>
                                <label>Jumlah</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="number" class="form-control" value="<?php echo $data['jumlah']; ?>" name="jumlah" />
                                    </div>
                                </div>
                                <label>Netto</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" value="<?php echo $data['netto']; ?>" name="nettot" />
                                    </div>
                                </div>
                                <label>Satuan</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" value="<?php echo $data['satuan']; ?>" name="satuan" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" name="simpan" class="btn btn-primary m-t-15 waves-effect">SIMPAN EDIT</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>