<div class="block-header">
    <button type="button" data-color="red" class="btn bg-red waves-effect" data-toggle="modal" data-target="#defaultModal">TAMBAH DATA</button>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    DATA BARANG MASUK
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th>No</th>
                                <th>Nama_Suplayer</th>
                                <th>Nama_Barang</th>
                                <th>Tanggal_BM</th>
                                <th>Jumlah_BM</th>
                                <th>Netto</th>
                                <th>Hasil_Test</th>
                                <th>Harga</th>
                                <th>Total_Harga</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $tampil = mysqli_query($connect, "SELECT tbl_barang_masuk.*, tbl_suplayer.*, tbl_stok_barang.* from tbl_barang_masuk
                            inner join tbl_suplayer on tbl_barang_masuk.kd_suplayer = tbl_suplayer.kd_suplayer
                            inner join tbl_stok_barang on tbl_barang_masuk.kode_barang = tbl_stok_barang.kode_barang
                            ORDER BY kd_BK ASC
                            ");
                            while ($row = mysqli_fetch_array($tampil)) {
                            ?>

                                <tr>
                                    <td>
                                        <div class="btn-group" role="group">
                                            <button id="btnGroupVerticalDrop1" type="button" class="btn bg-teal waves-effect dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="btnGroupVerticalDrop1">
                                                <li><a onClick="return confirm('Apa anda yakin?')" href="index.php?act=3&id=<?php echo $row['kd_BK']; ?>">Edit</a></li>
                                                <li><a onClick="return confirm('Apa anda yakin?')" href="../action/hapus_barang_masuk.php?id=<?php echo $row['kd_BK']; ?>&netto=<?php echo $row['netto']; ?>&jumlah=<?php echo $row['jumlah_BM']; ?>&id_brg=<?php echo $row['kode_barang']; ?>">Hapus</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $row['nama_suplayer']; ?></td>
                                    <td><?php echo $row['nama_barang']; ?></td>
                                    <td><?php echo $row['tanggal_BM']; ?></td>
                                    <td><?php echo $row['jumlah_BM']; ?>, <?php echo $row['satuan_stok']; ?></td>
                                    <td><?php echo $row['netto']; ?>, Kg</td>
                                    <td><?php echo $row['hasil_test']; ?></td>
                                    <td>Rp.<?php echo number_format($row['harga'], 0, ',', '.') ?></td>
                                    <td>Rp.<?php echo number_format($row['harga'] * $row['jumlah_BM'], 0, ',', '.') ?></td>

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Default Size -->
<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">TAMBAH DATA BARANG MASUK</h4>
            </div>
            <form action="../action/input_barang_masuk.php" method="post">
                <div class="modal-body" style="font-size: 11px;">
                    <div class="row clearfix">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <label>Suplayer</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <select class="form-control show-tick" data-show-subtext="true" required name="input_suplayer">
                                        <option value="0">-- Pilih --</option>
                                        <?php
                                        $tampilkan_Suplayer = mysqli_query($connect, "SELECT * FROM tbl_suplayer  ORDER BY kd_suplayer DESC");
                                        foreach ($tampilkan_Suplayer as $data_Suplayer) {
                                        ?>
                                            <option value="<?php echo $data_Suplayer['kd_suplayer']; ?>"><?php echo $data_Suplayer['nama_suplayer']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <label>Nama Barang</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <select class="form-control show-tick" data-show-subtext="true" required name="input_nama_barang">
                                        <option value="0">-- Pilih --</option>
                                        <?php
                                        $tampilkan_stok_barang = mysqli_query($connect, "SELECT * FROM tbl_stok_barang  ORDER BY kode_barang DESC");
                                        foreach ($tampilkan_stok_barang as $data_stok_barang) {
                                        ?>
                                            <option value="<?php echo $data_stok_barang['kode_barang']; ?>"><?php echo $data_stok_barang['nama_barang']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <label>Tanggal BM</label>
                            <div class="form-group">
                                <div class="input-group date" id="bs_datepicker_component_container">
                                    <div class="form-line">
                                        <input type="text" class="form-control" required name="tgl_bm">
                                    </div>
                                    <span class="input-group-addon">
                                        <i class="material-icons">date_range</i>
                                    </span>
                                </div>
                            </div>
                            <label>Jumlah BM</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="number" class="form-control" required name="jumlah_beli" />
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <label>Netto</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" required name="netto" />
                                </div>
                            </div>
                            <label>Hasil Test</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" required name="hasil_test" />
                                </div>
                            </div>
                            <label>Harga</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" required name="harga" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="simpan" class="btn btn-link waves-effect">SAVE</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </form>
        </div>
    </div>
</div>