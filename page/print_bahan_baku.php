<?php
session_start();
include_once '../config/koneksi.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        /*design table 1*/
        .table1 {
            font-family: sans-serif;
            color: #232323;
            border-collapse: collapse;
        }

        .table1,
        .th,
        .td {
            border: 1px solid #999;
            padding: 8px 20px;
            font-size: 12px;
        }
    </style>
</head>

<body onload="window.print()">
    <div class="table-responsive">
        <table border="0" style="width: 100%;">
            <tr>
                <td align="right" style="width: 30%"><img src="../images/logo.webp" alt="" style="width: 100px;height: 100px;"></td>
                <td align="center" style="width: 40%">
                    <h3><b>PT. MITRA AYU ADIPRATAMA</b> </h3>
                    <div><b><i>Manufacture to perfection</i></b></div>


                </td>
                <td align="right" style="width: 30%">
                    <!-- <img src="../images/logo.webp" alt="" style="width: 100px;height: 100px;"> -->
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center" style="font-size: 12px;">
                    <b>
                        <div>Jalan TPA BalaiGadang Air Dingin, Kel. BalaiGadangKec. Koto Tangah, Padang 25174 – Indonesia
                        </div>
                        <div>Phone: +62-751-7051619 ᵒ Fax: +62-751-25944 ᵒ info@ptmitraayu.com</</div> </b> <hr>
                </td>
            </tr>
        </table>
        <center>
            <div>
                <h4>LAPORAN STOK PERSEDIAAN</h4>
            </div>
        </center>
        <table style="width: 100%;text-align: center;" class="table1" border="0">
            <thead>
                <tr>
                    <th class="th">No</th>
                    <th class="th">Nama Barang</th>
                    <th class="th">Jumlah Barang</th>
                    <th class="th">Netto</th>
                    <th class="th">Ket</th>

                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                $tampil = mysqli_query($connect, "SELECT * from tbl_stok_barang ORDER BY kode_barang asc");
                while ($row = mysqli_fetch_array($tampil)) {
                ?>
                    <tr>
                        <td class="td"><?php echo $no++; ?></td>
                        <td class="td"><?php echo $row['nama_barang']; ?></td>
                        <td class="td"><?php echo $row['jumlah_barang_stok']; ?>, <?php echo $row['satuan_stok']; ?></td>
                        <td class="td"><?php echo $row['netto_stok']; ?>, Kg</td>
                        <td class="td"><?php echo $row['ket']; ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <br>
        <br>
        <br>
        <table border="0" align="right" class="">
            <tr>
                <td align="center">Padang, <?php echo date('d-F-Y');?></td>
            </tr>
            <tr>
                <td colspan="0" align="center">
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                </td>
            </tr>
            <tr>
                <td align="center">Maria Magdalena Alwi</td>
            </tr>
        </table>
    </div>
</body>

</html>