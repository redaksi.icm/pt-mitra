<?php
$no = 1;
$tampilkan = mysqli_query($connect, "SELECT * FROM tbl_stok_barang  where kode_barang = '$_GET[id]'");
foreach ($tampilkan as $data) {
?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        EDIT STOK BARANG MASUK
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <form action="../action/edit_stok.php?id=<?php echo $data['kode_barang']; ?>" method="post">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Nama Barang</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" required name="nama_barang" value="<?php echo $data['nama_barang']; ?>" />
                                    </div>
                                </div>
                                <label>Satuan Barang</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control show-tick" id="satuan_stok" name="satuan_stok">
                                            <option <?php if ($data['satuan_stok'] == 'Drum') {
                                                    }; ?> value="Drum">Drum</option>
                                            <option <?php if ($data['satuan_stok'] == 'Coli') {
                                                    }; ?> value="Coli">Coli</option>
                                        </select>
                                    </div>
                                </div>
                                <label>Ket</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" required name="ket" value="<?php echo $data['ket']; ?>" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" name="simpan" class="btn btn-primary m-t-15 waves-effect">SIMPAN EDIT</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>