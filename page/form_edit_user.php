<?php
$no = 1;
$tampil = mysqli_query($connect, "SELECT * FROM tbl_user where id_user ='$_GET[id]'");
foreach ($tampil as $data) {
?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        EDIT USER
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <form action="../action/input_edit_user.php?id=<?php echo $data['id_user'];?>" method="post">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label>Nama</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" value="<?php echo $data['nama']; ?>" name="nama_user" />
                                    </div>
                                </div>
                                <label>Alamat</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" value="<?php echo $data['alamat']; ?>" required name="alamat" />
                                    </div>
                                </div>
                                <label>Jabatan</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control show-tick" name="jabatan">
                                            <option value="0">-- Pilih --</option>
                                            <option <?php if ($data['jabatan'] == 'Karyawan') {
                                                        echo 'selected';
                                                    }; ?> value="Karyawan">Karyawan</option>
                                            <option <?php if ($data['jabatan'] == 'Marketing') {
                                                        echo 'selected';
                                                    }; ?> value="Marketing">Marketing</option>
                                            <option <?php if ($data['jabatan'] == 'Pimpinan') {
                                                        echo 'selected';
                                                    }; ?> value="Pimpinan">Pimpinan</option>
                                        </select>
                                    </div>
                                </div>
                                <label>No HP</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" value="<?php echo $data['no_hp']; ?>" required name="hp" />
                                    </div>
                                </div>

                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label>Email</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" value="<?php echo $data['email']; ?>" name="Email" />
                                    </div>
                                </div>
                                <label>Username</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" value="<?php echo $data['user']; ?>" name="username" />
                                    </div>
                                </div>
                                <label>Password</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" value="<?php echo $data['password']; ?>" name="password" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" name="simpan" class="btn btn-primary m-t-15 waves-effect">SIMPAN EDIT</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>