<div class="block-header">
    <button type="button" data-color="red" class="btn bg-red waves-effect" data-toggle="modal" data-target="#defaultModal">TAMBAH SUPLAYER</button>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    DATA SUPLAYER
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th>No</th>
                                <th>Nama Suplayer</th>
                                <th>Alamat</th>
                                <th>Jenis_Kelamin</th>
                                <th>No_HP</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $tampil = mysqli_query($connect, "SELECT * FROM tbl_suplayer order by kd_suplayer desc");
                            foreach ($tampil as $data) {
                            ?>
                                <tr>
                                    <td>
                                        <div class="btn-group" role="group">
                                            <button id="btnGroupVerticalDrop1" type="button" class="btn bg-teal waves-effect dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="btnGroupVerticalDrop1">
                                                <li><a onClick="return confirm('Apa anda yakin?')" href="index.php?act=7&id=<?php echo $data['kd_suplayer']; ?>">Edit</a></li>
                                                <li><a onClick="return confirm('Apa anda yakin?')" href="../action/hapus_suplayer.php?id=<?php echo $data['kd_suplayer']; ?>">Hapus</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $data['nama_suplayer']; ?></td>
                                    <td><?php echo $data['alamat']; ?></td>
                                    <td><?php if ($data['jk'] == 1) {
                                            echo 'Laki-laki';
                                        } else {
                                            echo 'Perempuan';
                                        } ?></td>
                                    <td><?php echo $data['no_hp']; ?></td>
                                    <td><?php if ($data['status'] == 1) {
                                            echo 'Aktif';
                                        } else {
                                            echo 'Tidak Aktif';
                                        }; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Default Size -->
<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">TAMBAH SUPLAYER</h4>
            </div>
            <div class="modal-body" style="font-size: 11px;">
                <form action="../action/input_suplayer.php" method="post">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>Nama Suplayer</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" required name="nama_suplayer" />
                                </div>
                            </div>
                            <label>Alamat</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" required name="alamat" />
                                </div>
                            </div>
                            <label>Jenis Kelamin</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <select class="form-control show-tick" required name="kelamin" style="width: 22px;">
                                        <option value="0">Pilih</option>
                                        <option value="1">Laki-laki</option>
                                        <option value="2">Perempuan</option>
                                    </select>
                                    <!-- <input type="text" class="form-control" required name="kelamin" /> -->
                                </div>
                            </div>
                            <label>No HP</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" required name="hp" />
                                </div>
                            </div>
                            <input type="checkbox" id="aktifkan" name="aktifkan" value="1" class="filled-in chk-col-red" checked />
                            <label for="aktifkan">Aktif</label>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" name="simpan" class="btn btn-link waves-effect">SAVE</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
            </form>
        </div>
    </div>
</div>