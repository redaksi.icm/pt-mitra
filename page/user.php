<div class="block-header">
    <button type="button" data-color="red" class="btn bg-red waves-effect" data-toggle="modal" data-target="#defaultModal">TAMBAH USER</button>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    DATA USER
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th>No</th>
                                <th>Nama</th>
                                <th>User</th>
                                <th>Password</th>
                                <th>Alamat</th>
                                <th>No_HP</th>
                                <th>Email</th>
                                <th>jabatan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $tampil = mysqli_query($connect, "SELECT * FROM tbl_user order by id_user desc");
                            foreach ($tampil as $data) {
                            ?>
                                <tr>
                                    <td>
                                        <div class="btn-group" role="group">
                                            <button id="btnGroupVerticalDrop1" type="button" class="btn bg-teal waves-effect dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="btnGroupVerticalDrop1">
                                                <li><a onClick="return confirm('Apa anda yakin?')" href="index.php?act=15&id=<?php echo $data['id_user']; ?>">Edit</a></li>
                                                <li><a onClick="return confirm('Apa anda yakin?')" href="../action/hapus_user.php?id=<?php echo $data['id_user']; ?>">Hapus</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $data['nama']; ?></td>
                                    <td><?php echo $data['user']; ?></td>
                                    <td><?php echo $data['password']; ?></td>
                                    <td><?php echo $data['alamat']; ?></td>
                                    <td><?php echo $data['no_hp']; ?></td>
                                    <td><?php echo $data['email']; ?></td>
                                    <td><?php echo $data['jabatan']; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Default Size -->
<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">TAMBAH USER</h4>
            </div>
            <div class="modal-body" style="font-size: 11px;">
                <form action="../action/input_user.php" method="post">
                    <div class="row clearfix">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Nama</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" required name="nama_user" />
                                </div>
                            </div>
                            <label>Alamat</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" required name="alamat" />
                                </div>
                            </div>
                            <label>Jabatan</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <select class="form-control show-tick" name="jabatan">
                                        <option value="0">-- Pilih --</option>
                                        <option value="Karyawan">Karyawan</option>
                                        <option value="Marketing">Marketing</option>
                                        <option value="Pimpinan">Pimpinan</option>
                                    </select>
                                </div>
                            </div>
                            <label>No HP</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" required name="hp" />
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Email</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" required name="Email" />
                                </div>
                            </div>
                            <label>Username</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" required name="username" />
                                </div>
                            </div>
                            <label>Password</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" required name="password" />
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" name="simpan" class="btn btn-link waves-effect">SAVE</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
            </form>
        </div>
    </div>
</div>