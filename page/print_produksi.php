<?php
session_start();
include_once '../config/koneksi.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        /*design table 1*/
        .table1 {
            font-family: sans-serif;
            color: #232323;
            border-collapse: collapse;
        }

        .table1,
        .th,
        .td {
            border: 1px solid #999;
            padding: 8px 20px;
            font-size: 12px;
        }
    </style>
</head>
<!--  -->

<body onload="window.print()">
    <div class="table-responsive">
        <table border="0" style="width: 100%;">
            <tr>
                <td align="right" style="width: 30%"><img src="../images/logo.webp" alt="" style="width: 100px;height: 100px;"></td>
                <td align="center" style="width: 40%">
                    <h3><b>PT. MITRA AYU ADIPRATAMA</b> </h3>
                    <div><b><i>Manufacture to perfection</i></b></div>

                </td>
                <td align="right" style="width: 30%">
                    <!-- <img src="../images/logo.webp" alt="" style="width: 100px;height: 100px;"> -->
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center" style="font-size: 12px;">
                    <b>
                        <div>Jalan TPA BalaiGadang Air Dingin, Kel. BalaiGadangKec. Koto Tangah, Padang 25174 – Indonesia
                        </div>
                        <div>Phone: +62-751-7051619 ᵒ Fax: +62-751-25944 ᵒ info@ptmitraayu.com</</div> </b> <hr>
                </td>
            </tr>
        </table>
        <center><div>
            <h4>LAPORAN HASIL PRODUKSI</h4>
        </div></center>
        <table style="width: 100%;text-align: center;" class="table1" border="0">
            <thead>
                <tr>
                    <th class="th">No</th>
                    <th class="th">Nama Barang</th>
                    <th class="th">Jumlah barang</th>
                    <th class="th">Netto</th>
                    <th class="th">Ket</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                $tampil = mysqli_query($connect, "SELECT * from tbl_hasil_produksi order by kd_produksi desc");
                foreach ($tampil as $data) {
                ?>
                    <tr>
                        <td class="td"><?php echo $no++ ?></td>
                        <td class="td"><?php echo $data['nama_barang_produksi'] ?></td>
                        <td class="td"><?php echo $data['jumlah_barang_pro'] ?>, <?php echo $data['satuan_produksi'] ?></td>
                        <td class="td"><?php echo $data['netto'] ?>, Kg</td>
                        <td class="td"><?php echo $data['ket_p'] ?></td>
                    </tr>

                <?php } ?>
            </tbody>
        </table>
        <br>
        <br>
        <br>
        <table border="0" align="right" class="">
            <tr>
                <td align="center">Padang, <?php echo date('d-F-Y');?></td>
            </tr>
            <tr>
                <td colspan="0" align="center">
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                </td>
            </tr>
            <tr>
                <td align="center">Maria Magdalena Alwi</td>
            </tr>
        </table>
    </div>
</body>

</html>