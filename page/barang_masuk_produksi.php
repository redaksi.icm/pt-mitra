<div class="block-header">
    <button type="button" data-color="red" class="btn bg-red waves-effect" data-toggle="modal" data-target="#defaultModal">TAMBAH DATA</button>
</div>


<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    DATA BARANG MASUK PRODUKSI
                </h2>
            </div>
            <div class="body">
                <div class="table-responsive">

                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th>No</th>
                                <th>Nama_Barang</th>
                                <th>Tanggal_Masuk</th>
                                <th>Jumlah</th>
                                <th>Netto</th>
                                <th>Hasil_Test</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $tampil = mysqli_query($connect, "SELECT * FROM tbl_barang_masuk_produksi
                            INNER JOIN tbl_hasil_produksi ON tbl_hasil_produksi.kd_produksi = tbl_barang_masuk_produksi.kd_produksi
                            ORDER BY tbl_barang_masuk_produksi.kd_bm_produksi DESC
                            ");
                            while ($row = mysqli_fetch_array($tampil)) {
                            ?>
                                <tr>
                                    <td>
                                        <div class="btn-group" role="group">
                                            <button id="btnGroupVerticalDrop1" type="button" class="btn bg-teal waves-effect dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="btnGroupVerticalDrop1">
                                                <li><a onClick="return confirm('Apa anda yakin?')" href="index.php?act=21&id=<?php echo $row['kd_bm_produksi']; ?>">Edit</a></li>
                                                <li><a onClick="return confirm('Apa anda yakin?')" href="../action/hapus_barang_masuk_produksi.php?id=<?php echo $row['kd_bm_produksi']; ?>&netto=<?php echo $row['netto_masuk']; ?>&jumlah=<?php echo $row['jumlah_masuk']; ?>&id_produksi=<?php echo $row['kd_produksi']; ?>">Hapus</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $row['nama_barang_produksi']; ?></td>
                                    <td><?php echo $row['tgl_masuk_produksi']; ?></td>
                                    <td><?php echo $row['jumlah_masuk']; ?>, <?php echo $row['satuan_produksi']; ?></td>
                                    <td><?php echo $row['netto_masuk']; ?>, Kg</td>
                                    <td><?php echo $row['hasil_test']; ?></td>
                                    <td><?php echo $row['ket_masuk']; ?></td>
                                </tr>

                            <?php } ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>



<!-- Default Size -->
<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">TAMBAH DATA BARANG MASUK</h4>
            </div>
            <form action="../action/input_barang_masuk_produksi.php" method="post">
                <div class="modal-body" style="font-size: 11px;">
                    <div class="row clearfix">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <label>Nama Barang</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <select class="form-control show-tick" data-show-subtext="true" required name="input_nama_barang">
                                        <option value="0">-- Pilih --</option>
                                        <?php
                                        $tampilkan_stok_barang = mysqli_query($connect, "SELECT * FROM tbl_hasil_produksi  ORDER BY kd_produksi DESC");
                                        foreach ($tampilkan_stok_barang as $data_stok_barang) {
                                        ?>
                                            <option value="<?php echo $data_stok_barang['kd_produksi']; ?>"><?php echo $data_stok_barang['nama_barang_produksi']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <label>Tanggal BM</label>
                            <div class="form-group">
                                <div class="input-group date" id="bs_datepicker_component_container">
                                    <div class="form-line">
                                        <input type="text" class="form-control" required name="tgl_bm">
                                    </div>
                                    <span class="input-group-addon">
                                        <i class="material-icons">date_range</i>
                                    </span>
                                </div>
                            </div>
                            <label>Jumlah BM</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="number" class="form-control" required name="jumlah_beli" />
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <label>Netto</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" required name="netto" />
                                </div>
                            </div>
                            <label>Hasil Test</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" required name="hasil_test" />
                                </div>
                            </div>
                            <label>Ket</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" required name="ket" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="simpan" class="btn btn-link waves-effect">SAVE</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </form>
        </div>
    </div>
</div>