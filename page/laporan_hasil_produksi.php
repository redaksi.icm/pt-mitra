<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    LAPORAN HASIL PRODUKSI
                </h2>
            </div>
            <form target="_blank" action="print_produksi.php" method="POST">
                <div class="row clearfix" style="padding: 10px;">
                    <div class="col-sm-3">
                        <div class="left" style="padding: 10px;">
                            <button type="submit" class="btn bg-red waves-effect">
                                <i class="material-icons">print</i>
                                <span>PRINT...</span>
                            </button>
                        </div>
                    </div>
                </div>

            </form>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Barang</th>
                                <th>Jumlah barang</th>
                                <th>Netto</th>
                                <th>Ket</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $tampil = mysqli_query($connect, "SELECT * from tbl_hasil_produksi order by kd_produksi desc");
                            foreach ($tampil as $data) {
                            ?>
                                <tr>
                                    <td><?php echo $no++ ?></td>
                                    <td><?php echo $data['nama_barang_produksi'] ?></td>
                                    <td><?php echo $data['jumlah_barang_pro'] ?>, <?php echo $data['satuan_produksi'] ?></td>
                                    <td><?php echo $data['netto'] ?>, Kg</td>
                                    <td><?php echo $data['ket_p'] ?></td>
                                </tr>

                            <?php } ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>