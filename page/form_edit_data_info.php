<?php
$no = 1;
$tampil = mysqli_query($connect, "SELECT * FROM tbl_info where kd_info='$_GET[id]'");
foreach ($tampil as $data) {
?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        EDIT DATA INFO
                    </h2>
                </div>
                <div class="body">
                    <form action="../action/edit_input_info.php?id=<?php echo $_GET['id'];?>" method="post" enctype="multipart/form-data">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Tgl Opname</label>
                                <div class="form-group">
                                    <div class="input-group date" id="bs_datepicker_component_container">
                                        <div class="form-line">
                                            <input type="text" class="form-control" value="<?php echo date('m/d/Y', strtotime($data['tgl_info'])); ?>" required name="tgl_info">
                                        </div>
                                        <span class="input-group-addon">
                                            <i class="material-icons">date_range</i>
                                        </span>
                                    </div>
                                </div>
                                <label>Judul Info</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" required value="<?php echo $data['judul']; ?>" name="judul" />
                                    </div>
                                </div>
                                <label>Info Gambar</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="file" class="form-control"  name="gambar" />
                                        <input type="text" class="form-control" value="<?php echo $data['gambar']; ?>" name="gambar1" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" name="simpan" class="btn btn-primary m-t-15 waves-effect">SIMPAN EDIT</button>

                    </form>

                </div>
            </div>
        </div>
    </div>
<?php } ?>