<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    LAPORAN STOK PERSEDIAAN
                </h2>
            </div>
            <div class="left" style="padding: 10px;">
                <a target="_blank" href="print_bahan_baku.php" class="btn bg-red waves-effect">
                    <i class="material-icons">print</i>
                    <span>PRINT...</span>
                </a>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Barang</th>
                                <th>Jumlah Barang</th>
                                <th>Netto</th>
                                <th>Ket</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $tampil = mysqli_query($connect, "SELECT * from tbl_stok_barang ORDER BY kode_barang asc");
                            while ($row = mysqli_fetch_array($tampil)) {
                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $row['nama_barang']; ?></td>
                                    <td><?php echo $row['jumlah_barang_stok']; ?>, <?php echo $row['satuan_stok']; ?></td>
                                    <td><?php echo $row['netto_stok']; ?>, Kg</td>
                                    <td><?php echo $row['ket']; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>