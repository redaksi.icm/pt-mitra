<?php
$id = $_SESSION['id_user'];
$tampilkan = mysqli_query($connect, "SELECT * FROM tbl_user  where id_user = '$id'");
foreach ($tampilkan as $data) {
?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        EDIT AKSES
                    </h2>
                </div>
                <div class="body">

                    <form action="../action/update_akses.php?id=<?php echo $id; ?>" method="post">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <label>Nama</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" required name="nama" value="<?php echo $data['nama']; ?>" />
                                    </div>
                                </div>
                                <label>No HP</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="number" class="form-control" required name="no_hp" value="<?php echo $data['no_hp']; ?>" />
                                    </div>
                                </div>
                                <label>Email</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" required name="email" value="<?php echo $data['email']; ?>" />
                                    </div>
                                </div>
                                <label>Username</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" required name="user" value="<?php echo $data['user']; ?>" />
                                    </div>
                                </div>
                                <label>Password</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="password" class="form-control" required name="password" value="<?php echo $data['password']; ?>" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" name="simpan" class="btn btn-primary m-t-15 waves-effect">SIMPAN EDIT</button>
                    </form>


                </div>
            </div>
        </div>
    </div>
<?php } ?>