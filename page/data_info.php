<div class="block-header">
    <button type="button" data-color="red" class="btn bg-red waves-effect" data-toggle="modal" data-target="#defaultModal">TAMBAH INFO</button>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    DATA INFO
                </h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th>No</th>
                                <th>Judul Info</th>
                                <th>Tgl Info</th>
                                <th>Info Gambar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $tampil = mysqli_query($connect, "SELECT *
                            FROM tbl_info order by kd_info desc
                        ");
                            foreach ($tampil as $data) {
                            ?>
                                <tr>
                                    <td>
                                        <div class="btn-group" role="group">
                                            <button id="btnGroupVerticalDrop1" type="button" class="btn bg-teal waves-effect dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="btnGroupVerticalDrop1">
                                                <li><a onClick="return confirm('Apa anda yakin?')" href="index.php?act=19&id=<?php echo $data['kd_info']; ?>">Edit</a></li>
                                                <li><a onClick="return confirm('Apa anda yakin?')" href="../action/hapus_info.php?id=<?php echo $data['kd_info']; ?>">Hapus</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $data['judul']; ?></td>
                                    <td><?php echo $data['tgl_info']; ?></td>
                                    <td><?php echo $data['gambar']; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- batas -->
<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">INFO</h4>
            </div>
            <div class="modal-body" style="font-size: 11px;">
                <form action="../action/input_info.php" method="post" enctype="multipart/form-data">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>Tgl Opname</label>
                            <div class="form-group">
                                <div class="input-group date" id="bs_datepicker_component_container">
                                    <div class="form-line">
                                        <input type="text" class="form-control" required name="tgl_info">
                                    </div>
                                    <span class="input-group-addon">
                                        <i class="material-icons">date_range</i>
                                    </span>
                                </div>
                            </div>
                            <label>Judul Info</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" required name="judul" />
                                </div>
                            </div>
                            <label>Info Gambar</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="file" class="form-control" required name="gambar" />
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" name="simpan" class="btn btn-link waves-effect">SAVE</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
            </form>
        </div>
    </div>
</div>