<?php
$no = 1;
$tampil = mysqli_query($connect, "SELECT * FROM tbl_barang_masuk_produksi
                            INNER JOIN tbl_hasil_produksi ON tbl_hasil_produksi.kd_produksi = tbl_barang_masuk_produksi.kd_produksi
                            WHERE tbl_barang_masuk_produksi.kd_bm_produksi = '$_GET[id]'
                            ");
while ($row = mysqli_fetch_array($tampil)) {
?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        EDIT DATA BARANG MASUK PRODUKSI
                    </h2>
                </div>
                <div class="body">
                    <form action="../action/edit_barang_masuk_produksi.php" method="post">
                        <div class="modal-body" style="font-size: 11px;">
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <label>Nama Barang</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" disabled name="input_nama_barang" value="<?php echo $row['nama_barang_produksi']; ?>" />
                                            <input type="hidden" class="form-control"  name="kd_bm_produksi" value="<?php echo $_GET['id']; ?>" />
                                        </div>
                                    </div>
                                    <label>Tanggal BM</label>
                                    <div class="form-group">
                                        <div class="input-group date" id="bs_datepicker_component_container">
                                            <div class="form-line">
                                                <input type="text" class="form-control" required name="tgl_bm" value="<?php echo date('m/d/Y', strtotime($row['tgl_masuk_produksi'])); ?>">
                                            </div>
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                        </div>
                                    </div>
                                    <label>Jumlah BM</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="number" class="form-control" required value="<?php echo $row['jumlah_masuk']; ?>" name="jumlah_beli" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <label>Netto</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" required value="<?php echo $row['netto_masuk']; ?>" name="netto" />
                                        </div>
                                    </div>
                                    <label>Hasil Test</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" required value="<?php echo $row['hasil_test']; ?>" name="hasil_test" />
                                        </div>
                                    </div>
                                    <label>Ket</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" value="<?php echo $row['ket_masuk']; ?>" required name="ket" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" name="simpan" class="btn btn-primary m-t-15 waves-effect">SIMPAN EDIT</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>