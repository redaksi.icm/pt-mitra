<div class="block-header">
    <a href="" data-color="red" class="btn bg-red waves-effect" data-toggle="modal" data-target="#largeModal">TAMBAH DATA</a>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    DATA BARANG KELUAR
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th>No</th>
                                <th>Nama_Barang</th>
                                <th>Tanggal_BM</th>
                                <th>Jumlah_Keluar</th>
                                <th>Netto</th>
                                <th>Tanggal_keluar</th>
                                <th>Tujuan_Keluar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $tampil = mysqli_query($connect, "SELECT * FROM 
                            tbl_barang_keluar 
                            INNER JOIN tbl_barang_masuk ON tbl_barang_masuk.kd_BK= tbl_barang_keluar.kd_bk
                            INNER JOIN tbl_stok_barang ON tbl_barang_masuk.kode_barang=tbl_stok_barang.kode_barang
                            ORDER BY kd_bm DESC ");
                            foreach ($tampil as $datakan) {
                            ?>
                                <tr>
                                    <td><a onClick="return confirm('Apa anda yakin?')" href="../action/hapus_barang_keluar.php?kd_bm=<?php echo $datakan['kd_bm']; ?>&jumlah=<?php echo $datakan['jumlah_bk']; ?>&kode_barang=<?php echo $datakan['kode_barang']; ?>&kd_BK=<?php echo $datakan['kd_BK']; ?>&netto_keluar=<?php echo $datakan['netto_keluar']; ?>" class="btn btn-danger waves-effect">Hapus</a></td>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $datakan['nama_barang']; ?></td>
                                    <td><?php echo $datakan['tanggal_BM']; ?></td>
                                    <td><?php echo $datakan['jumlah_bk']; ?>, <?php echo $datakan['satuan_stok']; ?></td>
                                    <td><?php echo $datakan['netto_keluar']; ?>, Kg</td>
                                    <td><?php echo $datakan['tanggal_bk']; ?></td>
                                    <td><?php echo $datakan['tujuan_keluar']; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- batsa -->
<!-- Large Size -->
<div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="largeModalLabel">TAMBAH DATA BARANG KELUAR</h4>
            </div>
            <div class="modal-body">
                <form action="../action/input_barang_keluar.php" method="post">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label>Nama Barang</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control show-tick" id="input_nama_barang" name="input_nama_barang" onchange="cariBarang()">
                                            <option value="0">-- Pilih --</option>
                                            <?php
                                            $tampilkan_stok_barang = mysqli_query($connect, "SELECT * FROM `tbl_barang_masuk` 
                                            INNER JOIN `tbl_stok_barang` ON `tbl_barang_masuk`.kode_barang=`tbl_stok_barang`.kode_barang 
                                            WHERE tbl_barang_masuk.kd_BK IN (SELECT MIN(tbl_barang_masuk.kd_BK) FROM  `tbl_barang_masuk` 
                                            LEFT JOIN story_barang_masuk_s ON story_barang_masuk_s.kd_BK = tbl_barang_masuk.kd_BK
                                            where story_barang_masuk_s.jumlah_masuk_s > 0  GROUP BY kode_barang)");
                                            foreach ($tampilkan_stok_barang as $data_stok_barang) {
                                            ?>
                                                <option value="<?php echo $data_stok_barang['kd_BK']; ?>"><?php echo $data_stok_barang['nama_barang']; ?> || TGL : <?php echo $data_stok_barang['tanggal_BM']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <label>Tanggal Masuk</label>
                                <div class="form-group">
                                    <div class="input-group date" id="">
                                        <div class="form-line">
                                            <input type="text" class="form-control" disabled name="tgl_bm" id="tgl_bm" style="background: #c1c1c1;">
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-md-6">
                                        <label>Barang Masuk</label>
                                        <div class="form-group">
                                            <div class="input-group date" id="">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" readonly name="stok_masuk" id="stok_masuk" style="background: #c1c1c1;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Netto Masuk</label>
                                        <div class="form-group">
                                            <div class="input-group date" id="">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" readonly name="netto_masuk" id="netto_masuk" style="background: #c1c1c1;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-6">
                                        <label>Sisa Barang</label>
                                        <div class="form-group">
                                            <div class="input-group date" id="">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" readonly name="tot_stok_keluar" id="tot_stok_keluar" style="background: #c1c1c1;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Sisa Netto</label>
                                        <div class="form-group">
                                            <div class="input-group date" id="">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" readonly name="tot_netto_keluar" id="tot_netto_keluar" style="background: #c1c1c1;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <label>Harga Barang</label>
                                <div class="form-group">
                                    <div class="input-group date" id="">
                                        <div class="form-line">
                                            <input type="text" class="form-control" readonly name="harga_barang" id="harga_barang" style="background: #c1c1c1;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label>Masukan Jumlah</label>
                                <div class="form-group">
                                    <div class="input-group date" id="">
                                        <div class="form-line">
                                            <input type="number" class="form-control" required name="jumlah_keluar" id="jumlah_keluar">
                                        </div>
                                    </div>
                                </div>
                                <label>Masukan Netto</label>
                                <div class="form-group">
                                    <div class="input-group date" id="">
                                        <div class="form-line">
                                            <input type="number" class="form-control" required name="netto_keluar" id="netto_keluar">
                                        </div>
                                    </div>
                                </div>
                                <label>Tanggal Barang Keluar</label>
                                <div class="form-group">
                                    <div class="input-group date" id="bs_datepicker_component_container">
                                        <div class="form-line">
                                            <input type="text" class="form-control" required name="tgl_bm">
                                        </div>
                                        <span class="input-group-addon">
                                            <i class="material-icons">date_range</i>
                                        </span>
                                    </div>
                                </div>
                                <label>Tujuan Keluar</label>
                                <div class="form-group">
                                    <div class="input-group date" id="">
                                        <div class="form-line">
                                            <input type="text" class="form-control" required name="tujuan_barang" id="tujuan_barang">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" name="simpan" class="btn btn-link waves-effect">SAVE</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
            </form>
        </div>
    </div>
</div>