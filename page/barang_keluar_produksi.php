<div class="block-header">
    <a href="" data-color="red" class="btn bg-red waves-effect" data-toggle="modal" data-target="#largeModal">TAMBAH DATA</a>
</div>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    DATA BARANG KELUAR PRODUKSI
                </h2>
            </div>
            <div class="body">
                <div class="table-responsive">

                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th>No</th>
                                <th>Nama_Barang</th>
                                <th>Tgl_masuk</th>
                                <th>Jumlah_Keluar</th>
                                <th>Netto</th>
                                <th>Tanggal_keluar</th>
                                <th>Tujuan_Keluar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $tampil = mysqli_query($connect, "SELECT * FROM 
                            tbl_barang_keluar_produksi 
                            INNER JOIN tbl_barang_masuk_produksi ON tbl_barang_masuk_produksi.kd_bm_produksi= tbl_barang_keluar_produksi.kd_bm_produksi
                            INNER JOIN tbl_hasil_produksi ON tbl_barang_masuk_produksi.kd_produksi=tbl_hasil_produksi.kd_produksi
                            ORDER BY kd_keluar_produksi  DESC ");
                            foreach ($tampil as $datakan) {
                            ?>

                                <tr>
                                    <td>
                                        <a onClick="return confirm('Apa anda yakin?')" href="../action/hapus_barang_keluar_produksi.php?id=<?php echo $datakan['kd_keluar_produksi']; ?>&jumlah=<?php echo $datakan['jumlah_keluar_produksi']; ?>&kd_produksi=<?php echo $datakan['kd_produksi']; ?>&netto_keluar=<?php echo $datakan['netto_keluar_produksi']; ?>&kd_bm_produksi=<?php echo $datakan['kd_bm_produksi']; ?>" class="btn btn-danger waves-effect">Hapus</a>
                                    </td>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $datakan['nama_barang_produksi']; ?></td>
                                    <td><?php echo $datakan['tgl_masuk_produksi']; ?></td>
                                    <td><?php echo $datakan['jumlah_keluar_produksi']; ?>, <?php echo $datakan['satuan_produksi']; ?></td>
                                    <td><?php echo $datakan['netto_keluar_produksi']; ?>, Kg</td>
                                    <td><?php echo $datakan['tgl_keluar_produksi']; ?></td>
                                    <td><?php echo $datakan['tujuan_keluar_produksi']; ?></td>
                                </tr>

                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>




<!-- batas -->
<div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="largeModalLabel">TAMBAH DATA BARANG KELUAR PRODUKSI</h4>
            </div>
            <div class="modal-body">
                <form action="../action/input_barang_keluar_produksi.php" method="post">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label>Nama Barang</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control show-tick" id="kode_produksi" name="kode_produksi" onchange="cariBarang_produksi()">
                                            <option value="0">-- Pilih --</option>
                                            <?php
                                            $tampilkan_stok_barang = mysqli_query($connect, "SELECT * FROM tbl_barang_masuk_produksi 
                                            INNER JOIN tbl_hasil_produksi ON tbl_barang_masuk_produksi.kd_produksi=tbl_hasil_produksi.kd_produksi 
                                            WHERE tbl_barang_masuk_produksi.kd_bm_produksi IN (SELECT MIN(tbl_barang_masuk_produksi.kd_bm_produksi) FROM tbl_barang_masuk_produksi
                                            LEFT JOIN story_barang_produksi_s ON story_barang_produksi_s.kd_bm_produksi = tbl_barang_masuk_produksi.kd_bm_produksi 
                                            where jumlah_masuk_p > 0   GROUP BY kd_produksi)");
                                            foreach ($tampilkan_stok_barang as $data_stok_barang) {
                                            ?>
                                                <option value="<?php echo $data_stok_barang['kd_bm_produksi']; ?>"><?php echo $data_stok_barang['nama_barang_produksi']; ?> || TGL : <?php echo $data_stok_barang['tgl_masuk_produksi']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <label>Tanggal Masuk</label>
                                <div class="form-group">
                                    <div class="input-group date" id="">
                                        <div class="form-line">
                                            <input type="text" class="form-control" disabled name="tgl_bm_produksi" id="tgl_bm_produksi" style="background: #c1c1c1;">
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <label>Barang Masuk</label>
                                        <div class="form-group">
                                            <div class="input-group date" id="">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" readonly name="stok_masuk_produksi" id="stok_masuk_produksi" style="background: #c1c1c1;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <label>Netto</label>
                                        <div class="form-group">
                                            <div class="input-group date" id="">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" readonly name="netto_masuk_produksi" id="netto_masuk_produksi" style="background: #c1c1c1;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <label>Sisa Barang</label>
                                        <div class="form-group">
                                            <div class="input-group date" id="">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" readonly name="sisa_stok_sisa" id="sisa_stok_sisa" style="background: #c1c1c1;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <label>Sisa Netto</label>
                                        <div class="form-group">
                                            <div class="input-group date" id="">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" readonly name="sisa_netto_sisa" id="sisa_netto_sisa" style="background: #c1c1c1;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label>Masukan Jumlah</label>
                                <div class="form-group">
                                    <div class="input-group date" id="">
                                        <div class="form-line">
                                            <input type="number" class="form-control" required name="jumlah_keluar_produksi" id="jumlah_keluar_produksi">
                                        </div>
                                    </div>
                                </div>
                                <label>Masukan Netto</label>
                                <div class="form-group">
                                    <div class="input-group date" id="">
                                        <div class="form-line">
                                            <input type="number" class="form-control" required name="netto_keluar_produksi" id="netto_keluar_produksi">
                                        </div>
                                    </div>
                                </div>
                                <label>Tanggal Barang Keluar</label>
                                <div class="form-group">
                                    <div class="input-group date" id="bs_datepicker_component_container">
                                        <div class="form-line">
                                            <input type="text" class="form-control" required name="tgl_bm_produksi">
                                        </div>
                                        <span class="input-group-addon">
                                            <i class="material-icons">date_range</i>
                                        </span>
                                    </div>
                                </div>
                                <label>Tujuan Keluar</label>
                                <div class="form-group">
                                    <div class="input-group date" id="">
                                        <div class="form-line">
                                            <input type="text" class="form-control" required name="tujuan_barang_produksi" id="tujuan_barang_produksi">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" name="simpan" class="btn btn-link waves-effect">SAVE</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
            </form>
        </div>
    </div>
</div>