<?php
$no = 1;
$tampilkan = mysqli_query($connect, "SELECT * FROM tbl_barang_masuk  where kd_BK = '$_GET[id]'");
foreach ($tampilkan as $data) {
?>

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        EDIT DATA BARANG MASUK
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <form action="../action/edit_barang_masuk.php?id=<?php echo $data['kd_BK']; ?>" method="post">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <label>Suplayer</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control show-tick" disabled data-show-subtext="true" required name="input_suplayer">
                                            <option value="0">-- Pilih --</option>
                                            <?php
                                            $tampilkan_Suplayer = mysqli_query($connect, "SELECT * FROM tbl_suplayer  ORDER BY kd_suplayer DESC");
                                            foreach ($tampilkan_Suplayer as $data_Suplayer) {
                                            ?>
                                                <option <?php if ($data_Suplayer['kd_suplayer'] == $data['kd_suplayer']) {
                                                            echo 'selected';
                                                        } else {
                                                            echo '';
                                                        } ?> value="<?php echo $data_Suplayer['kd_suplayer']; ?>"><?php echo $data_Suplayer['nama_suplayer']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <label>Nama Barang</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control show-tick" disabled data-show-subtext="true" required name="input_nama_barang">
                                            <option value="0">-- Pilih --</option>
                                            <?php
                                            $tampilkan_stok_barang = mysqli_query($connect, "SELECT * FROM tbl_stok_barang  ORDER BY kode_barang DESC");
                                            foreach ($tampilkan_stok_barang as $data_stok_barang) {
                                            ?>
                                                <option <?php if ($data_stok_barang['kode_barang'] == $data['kode_barang']) {
                                                            echo 'selected';
                                                        } else {
                                                            echo '';
                                                        } ?> value="<?php echo $data_stok_barang['kode_barang']; ?>"><?php echo $data_stok_barang['nama_barang']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <label>Tanggal BM</label>
                                <div class="form-group">
                                    <div class="input-group date" id="bs_datepicker_component_container">
                                        <div class="form-line">
                                            <input type="text" class="form-control" required name="tgl_bm" value="<?php echo date('m/d/Y', strtotime($data['tanggal_BM'])); ?>">
                                        </div>
                                        <span class="input-group-addon">
                                            <i class="material-icons">date_range</i>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <label>Hasil Test</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" required name="hasil_test" value="<?php echo $data['hasil_test']; ?>" />
                                    </div>
                                </div>
                                <label>Harga</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" required name="harga" value="<?php echo $data['harga']; ?>" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" name="simpan" class="btn btn-primary m-t-15 waves-effect">SIMPAN EDIT</button>
                </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>