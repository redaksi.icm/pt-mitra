<?php
$no = 1;
$tampil = mysqli_query($connect, "SELECT * FROM tbl_hasil_produksi where kd_produksi='$_GET[id]'");
foreach ($tampil as $data) {
?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        EDIT OPNAME
                    </h2>
                </div>
                <div class="body">
                    <form action="../action/edit_produksi.php?id=<?php echo $data['kd_produksi']; ?>" method="post">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Nama Barang</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control"  value="<?php echo $data['nama_barang_produksi']; ?>" name="barang">
                                    </div>
                                </div>
                                <label>Satuan Barang</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control show-tick" id="satuan_stok_produksi" name="satuan_stok_produksi">
                                            <option <?php if ($data['satuan_produksi'] == 'Drum') {
                                                    }; ?> value="Drum">Drum</option>
                                            <option <?php if ($data['satuan_produksi'] == 'Coli') {
                                                    }; ?> value="Coli">Coli</option>
                                        </select>
                                    </div>
                                </div>
                                <label>Keterangan</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" value="<?php echo $data['ket_p']; ?>" name="ket" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" name="simpan" class="btn btn-primary m-t-15 waves-effect">SIMPAN EDIT</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>