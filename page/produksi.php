<div class="block-header">
    <button type="button" data-color="red" class="btn bg-red waves-effect" data-toggle="modal" data-target="#defaultModal">TAMBAH DATA</button>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    STOK BARANG PRODUKSI
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th>No</th>
                                <th>Nama Barang</th>
                                <th>Jumlah barang</th>
                                <th>Netto</th>
                                <th>Ket</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $tampil = mysqli_query($connect, "SELECT * from tbl_hasil_produksi order by kd_produksi desc");
                            foreach ($tampil as $data) {
                            ?>
                                <tr>
                                    <td>
                                        <div class="btn-group" role="group">
                                            <button id="btnGroupVerticalDrop1" type="button" class="btn bg-teal waves-effect dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="btnGroupVerticalDrop1">
                                                <li><a onClick="return confirm('Apa anda yakin?')" href="index.php?act=11&id=<?php echo $data['kd_produksi']; ?>">Edit</a></li>
                                                <li><a onClick="return confirm('Apa anda yakin?')" href="../action/hapus_stok_produksi.php?id=<?php echo $data['kd_produksi']; ?>">Hapus</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                    <td><?php echo $no++ ?></td>
                                    <td><?php echo $data['nama_barang_produksi'] ?></td>
                                    <td><?php echo $data['jumlah_barang_pro'] ?>, <?php echo $data['satuan_produksi'] ?></td>
                                    <td><?php echo $data['netto'] ?>, Kg</td>
                                    <td><?php echo $data['ket_p'] ?></td>
                                </tr>

                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- batas -->
<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">PRODUKSI</h4>
            </div>
            <div class="modal-body" style="font-size: 11px;">
                <form action="../action/input_produksi.php" method="post">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>Nama Barang</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" required name="barang" />
                                </div>
                            </div>

                            <label>Satuan Barang</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <select class="form-control show-tick" id="satuan_stok_produksi" name="satuan_stok_produksi">
                                        <option value="0">-- Pilih --</option>
                                        <option value="Drum">Drum</option>
                                        <option value="Coli">Coli</option>
                                    </select>
                                </div>
                            </div>

                            <!-- <div class="form-group">
                                <div class="form-line">
                                    <select class="form-control show-tick" required name="barang" style="width: 22px;">
                                        <option value="0">Pilih</option>
                                        <?php
                                        $tampil_barang = mysqli_query($connect, "SELECT * FROM tbl_stok_barang");
                                        foreach ($tampil_barang as $data_barang) {
                                        ?>
                                            <option value="<?php echo $data_barang['kode_barang']; ?>"><?php echo $data_barang['nama_barang']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div> -->
                            <!-- <label>Jumlah Barang</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" required name="jumlah_barang_pro" />
                                </div>
                            </div>
                            <label>Netto</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" required name="nettot" />
                                </div>
                            </div> -->
                            <label>Keterangan</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" required name="ket" />
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" name="simpan" class="btn btn-link waves-effect">SAVE</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
            </form>
        </div>
    </div>
</div>