<div class="block-header">
    <button type="button" data-color="red" class="btn bg-red waves-effect" data-toggle="modal" data-target="#defaultModal">TAMBAH DATA</button>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    STOK BARANG
                </h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th>No</th>
                                <th>Nama Barang</th>
                                <th>Jumlah Barang</th>
                                <th>Netto</th>
                                <th>Ket</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $tampil = mysqli_query($connect, "SELECT * from tbl_stok_barang ORDER BY kode_barang asc");
                            while ($row = mysqli_fetch_array($tampil)) {
                            ?>
                                <tr>
                                    <td>
                                        <div class="btn-group" role="group">
                                            <button id="btnGroupVerticalDrop1" type="button" class="btn bg-teal waves-effect dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="btnGroupVerticalDrop1">
                                                <li><a onClick="return confirm('Apa anda yakin?')" href="index.php?act=5&id=<?php echo $row['kode_barang']; ?>">Edit</a></li>
                                                <li><a onClick="return confirm('Apa anda yakin?')" href="../action/hapus_stok.php?id=<?php echo $row['kode_barang']; ?>">Hapus</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $row['nama_barang']; ?></td>
                                    <td><?php echo $row['jumlah_barang_stok']; ?>, <?php echo $row['satuan_stok']; ?></td>
                                    <td><?php echo $row['netto_stok']; ?>, Kg</td>
                                    <td><?php echo $row['ket']; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Default Size -->
<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">TAMBAH STOK BARANG</h4>
            </div>
            <div class="modal-body" style="font-size: 11px;">
                <form action="../action/input_stok.php" method="post">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>Nama Barang</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" required name="nama_barang" />
                                </div>
                            </div>
                            <label>Satuan Barang</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <select class="form-control show-tick" id="satuan_stok" name="satuan_stok">
                                        <option value="0">-- Pilih --</option>
                                        <option value="Drum">Drum</option>
                                        <option value="Coli">Coli</option>
                                    </select>
                                </div>
                            </div>
                            <label>Ket</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" required name="ket" />
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" name="simpan" class="btn btn-link waves-effect">SAVE</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
            </form>
        </div>
    </div>
</div>