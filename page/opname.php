<!-- <div class="block-header">
    <button type="button" data-color="red" class="btn bg-red waves-effect" data-toggle="modal" data-target="#defaultModal">TAMBAH OPNAME</button>
</div> -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    STOK OPNAME
                </h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th>Nama Barang</th>
                                <th>Jumlah Barang</th>
                                <th>Netto Barang</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $tampil = mysqli_query($connect, "SELECT * FROM tbl_stok_barang");
                            foreach ($tampil as $data) {
                            ?>
                                <tr>
                                    <td><?php echo $data['nama_barang']; ?></td>
                                    <td><?php echo $data['jumlah_barang_stok']; ?>, <?php echo $data['satuan_stok']; ?></td>
                                    <td><?php echo $data['netto_stok']; ?>, Kg</td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    STOK OPNAME PRODUKSI
                </h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th>Nama Barang</th>
                                <th>Jumlah Barang</th>
                                <th>Netto Barang</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $tampil = mysqli_query($connect, "SELECT * FROM tbl_hasil_produksi");
                            foreach ($tampil as $data) {
                            ?>
                                <tr>
                                    <td><?php echo $data['nama_barang_produksi']; ?></td>
                                    <td><?php echo $data['jumlah_barang_pro']; ?>, <?php echo $data['satuan_produksi']; ?></td>
                                    <td><?php echo $data['netto']; ?>, Kg</td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- batas -->
<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">OPNAME</h4>
            </div>
            <div class="modal-body" style="font-size: 11px;">
                <form action="../action/input_opname.php" method="post">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>Nama Barang</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <select class="form-control show-tick" required name="barang" style="width: 22px;">
                                        <option value="0">Pilih</option>
                                        <?php
                                        $tampil_barang = mysqli_query($connect, "SELECT * FROM tbl_stok_barang");
                                        foreach ($tampil_barang as $data_barang) {
                                        ?>
                                            <option value="<?php echo $data_barang['kode_barang']; ?>"><?php echo $data_barang['nama_barang']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <!-- <input type="text" class="form-control" required name="kelamin" /> -->
                                </div>
                            </div>
                            <label>Tgl Opname</label>
                            <div class="form-group">
                                <div class="input-group date" id="bs_datepicker_component_container">
                                    <div class="form-line">
                                        <input type="text" class="form-control" required name="tgl_opname">
                                    </div>
                                    <span class="input-group-addon">
                                        <i class="material-icons">date_range</i>
                                    </span>
                                </div>
                            </div>
                            <label>Jumlah</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="number" class="form-control" required name="jumlah" />
                                </div>
                            </div>
                            <label>Netto</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" required name="nettot" />
                                </div>
                            </div>
                            <label>Satuan</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" required name="satuan" />
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" name="simpan" class="btn btn-link waves-effect">SAVE</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
            </form>
        </div>
    </div>
</div>