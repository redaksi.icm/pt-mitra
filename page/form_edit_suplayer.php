<?php
$no = 1;
$tampil = mysqli_query($connect, "SELECT * FROM tbl_suplayer where kd_suplayer = '$_GET[id]'");
foreach ($tampil as $data) {
?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        EDIT SUPLAYER
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <form action="../action/edit_suplayer.php?id=<?php echo $data['kd_suplayer'];?>" method="post">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Nama Suplayer</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" value="<?php echo $data['nama_suplayer']; ?>" name="nama_suplayer" />
                                    </div>
                                </div>
                                <label>Alamat</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" value="<?php echo $data['alamat']; ?>" name="alamat" />
                                    </div>
                                </div>
                                <label>Jenis Kelamin</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control show-tick" required name="kelamin" style="width: 22px;">
                                            <option <?php if ($data['jk'] == 1) {
                                                        echo 'selected';
                                                    } else {
                                                        echo '';
                                                    } ?> value="1">Laki-laki</option>
                                            <option <?php if ($data['jk'] == 2) {
                                                        echo 'selected';
                                                    } else {
                                                        echo '';
                                                    } ?> value="2">Perempuan</option>
                                        </select>
                                        <!-- <input type="text" class="form-control" required name="kelamin" /> -->
                                    </div>
                                </div>
                                <label>No HP</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" value="<?php echo $data['no_hp']; ?>" name="hp" />
                                    </div>
                                </div>
                                <input type="checkbox" id="aktifkan" name="aktifkan" value="1" class="filled-in chk-col-red" <?php if ($data['status'] == '1') {
                                                                                                                                    echo 'checked';
                                                                                                                                } else {
                                                                                                                                    echo '';
                                                                                                                                } ?> />
                                <label for="aktifkan">Aktif</label>
                            </div>
                        </div>
                        <button type="submit" name="simpan" class="btn btn-primary m-t-15 waves-effect">SIMPAN EDIT</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>