<div class="menu">
    <ul class="list">
        <li class="header">MAIN NAVIGATION</li>

        <?php if ($jabatan_view == 'Pimpinan') { ?>
            <li class="" id="0" onclick="aktifMenu(0)">
                <a href="index.php">
                    <i class="material-icons">home</i>
                    <span>Home</span>
                </a>
            </li>
            <li>
                <a href="index.php?act=17">
                    <i class="material-icons">hourglass_full</i>
                    <span>Laporan Produksi</span>
                </a>
            </li>
            <li>
                <a href="index.php?act=16">
                    <i class="material-icons">assignment</i>
                    <span>Laporan Stok Persediaan</span>
                </a>
            </li>


        <?php } else if ($jabatan_view == 'Karyawan') { ?>
            <li>
                <a href="index.php">
                    <i class="material-icons">home</i>
                    <span>Home</span>
                </a>
            </li>
            <li class="header">BARANG DARI SUPLAYER</li>
            <li>
                <a href="index.php?act=2">
                    <i class="material-icons">add_shopping_cart</i>
                    <span>Barang Masuk Suplayer</span>
                </a>
            </li>
            <li>
                <a href="index.php?act=12">
                    <i class="material-icons">remove_shopping_cart</i>
                    <span>Barang Keluar Suplayer</span>
                </a>
            </li>
            <li>
                <a href="index.php?act=4">
                    <i class="material-icons">assignment</i>
                    <span>Stok Barang Persediaan</span>
                </a>
            </li>
            <li class="header">BARANG PRODUKSI</li>

            <li>
                <a href="index.php?act=20">
                    <i class="material-icons">add_shopping_cart</i>
                    <span>Barang Masuk Produksi</span>
                </a>
            </li>
            <li>
                <a href="index.php?act=22">
                    <i class="material-icons">remove_shopping_cart</i>
                    <span>Barang Keluar Produksi</span>
                </a>
            </li>

            <li>
                <a href="index.php?act=10">
                    <i class="material-icons">refresh</i>
                    <span>Stok Barang Produksi</span>
                </a>
            </li>
            <li class="header">INFO</li>
            <li>
                <a href="index.php?act=6">
                    <i class="material-icons">person_add</i>
                    <span>Suplayer</span>
                </a>
            </li>
            <li>
                <a href="index.php?act=8">
                    <i class="material-icons">done_all</i>
                    <span>Opname</span>
                </a>
            </li>
            <li>
                <a href="index.php?act=18">
                    <i class="material-icons">announcement</i>
                    <span>Info</span>
                </a>
            </li>
            <li>
                <a href="index.php?act=14">
                    <i class="material-icons">people</i>
                    <span>User</span>
                </a>
            </li>
        <?php } else if ($jabatan_view == 'Marketing') { ?>
            <li class="" id="0" onclick="aktifMenu(0)">
                <a href="index.php">
                    <i class="material-icons">home</i>
                    <span>Home</span>
                </a>
            </li>
            <li>
                <a href="index.php?act=17">
                    <i class="material-icons">hourglass_full</i>
                    <span>Laporan Produksi</span>
                </a>
            </li>
            <!-- <li>
                <a href="index.php?act=16">
                    <i class="material-icons">assignment</i>
                    <span>Laporan Stok Persediaan</span>
                </a>
            </li> -->
        <?php } ?>


    </ul>
</div>