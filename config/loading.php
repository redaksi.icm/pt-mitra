<?php
include "koneksi.php";

if (isset($_GET["act"])) {
    $p = $_GET["act"];
} else {
    $p = 1;
}
switch ($p) {

    case "1":
        require("../page/beranda.php");
        break;

    case "2":
        require("../page/barang_masuk.php");
        break;

    case "3":
        require("../page/form_edit_b_masuk.php");
        break;

    case "4":
        require("../page/stok_barang.php");
        break;

    case "5":
        require("../page/form_edit_stok.php");
        break;

    case "6":
        require("../page/suplayer.php");
        break;

    case "7":
        require("../page/form_edit_suplayer.php");
        break;

    case "8":
        require("../page/opname.php");
        break;

    case "9":
        require("../page/form_edit_opname.php");
        break;

    case "10":
        require("../page/produksi.php");
        break;

    case "11":
        require("../page/form_edit_produksi.php");
        break;

    case "12":
        require("../page/barang_keluar.php");
        break;

    case "13":
        require("../page/tambah_barang_keluar.php");
        break;

    case "14":
        require("../page/user.php");
        break;

    case "15":
        require("../page/form_edit_user.php");
        break;

    case "16":
        require("../page/L_bahan_baku_stok.php");
        break;

    case "17":
        require("../page/laporan_hasil_produksi.php");
        break;

    case "18":
        require("../page/data_info.php");
        break;

    case "19":
        require("../page/form_edit_data_info.php");
        break;

    case "20":
        require("../page/barang_masuk_produksi.php");
        break;

    case "21":
        require("../page/form_edit_masuk_produksi.php");
        break;


    case "22":
        require("../page/barang_keluar_produksi.php");
        break;

    case "23":
        require("../page/akses.php");
        break;
}
